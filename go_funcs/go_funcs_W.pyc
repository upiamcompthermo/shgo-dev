�
�=�Wc           @` s�   d  d l  m Z m Z m Z d  d l m Z m Z m Z m Z m	 Z	 m
 Z
 d d l m Z d e f d �  �  YZ d e f d �  �  YZ d	 e f d
 �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d S(   i    (   t   divisiont   print_functiont   absolute_import(   t
   atleast_2dt   aranget   sumt   cost   expt   pii   (   t	   Benchmarkt   Watsonc           B` s#   e  Z d  Z d d � Z d �  Z RS(   sm  
    Watson objective function.

    This class defines the Watson [1]_ global optimization problem. This is a
    unimodal minimization problem defined as follows:

    .. math::

        f_{\text{Watson}}(x) = \sum_{i=0}^{29} \left\{
                               \sum_{j=0}^4 ((j + 1)a_i^j x_{j+1})
                               - \left[ \sum_{j=0}^5 a_i^j
                               x_{j+1} \right ]^2 - 1 \right\}^2
                               + x_1^2


    Where, in this exercise, :math:`a_i = i/29`.

    with :math:`x_i \in [-5, 5]` for :math:`i = 1, ..., 6`.

    *Global optimum*: :math:`f(x) = 0.002288` for
    :math:`x = [-0.0158, 1.012, -0.2329, 1.260, -1.513, 0.9928]`

    .. [1] Jamil, M. & Yang, X.-S. A Literature Survey of Benchmark Functions
    For Global Optimization Problems Int. Journal of Mathematical Modelling
    and Numerical Optimisation, 2013, 4, 150-194.

    TODO Jamil #161 writes equation using (j - 1).  According to code in Adorio
    and Gavana it should be (j+1). However the equations in those papers
    contain (j - 1) as well.  However, I've got the right global minimum!!!
    i   c         C` sg   t  j |  | � t t d g |  j d g |  j � � |  _ d d d d d d g g |  _ d	 |  _ d  S(
   Ng      �g      @gvq�-��g��x�&1�?g䃞ͪ�Ϳg)\���(�?gh��|?5��gi o���?gd?��H�b?(   R	   t   __init__t   listt   zipt   Nt   _boundst   global_optimumt   fglob(   t   selft
   dimensions(    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR   )   s
    ,c   
      G` s�   |  j  d 7_  t t d � � j } | d } t d � } t d � } t | d | | | d d d �} t | | | d d �} | | d d d }	 t |	 � | d d S(	   Ni   g      >@g      =@g      @g      @t   axisi   i    (   t   nfevR   R   t   TR   (
   R   t   xt   argst   it   at   jt   kt   t1t   t2t   inner(    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyt   fun2   s    
&(   t   __name__t
   __module__t   __doc__R   R    (    (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR
      s   	t   Wavyc           B` s#   e  Z d  Z d d � Z d �  Z RS(   s  
    Wavy objective function.

    This class defines the W / Wavy [1]_ global optimization problem. This is a
    multimodal minimization problem defined as follows:

    .. math::

        f_{\text{Wavy}}(x) = 1 - \frac{1}{n} \sum_{i=1}^{n}
                             \cos(kx_i)e^{-\frac{x_i^2}{2}}


    Where, in this exercise, :math:`k = 10`. The number of local minima is
    :math:`kn` and :math:`(k + 1)n` for odd and even :math:`k` respectively.

    Here, :math:`x_i \in [-\pi, \pi]` for :math:`i = 1, 2`.

    *Global optimum*: :math:`f(x) = 0` for :math:`x = [0, 0]`

    .. [1] Jamil, M. & Yang, X.-S. A Literature Survey of Benchmark Functions
    For Global Optimization Problems Int. Journal of Mathematical Modelling
    and Numerical Optimisation, 2013, 4, 150-194.
    i   c         C` s{   t  j |  | � t t t g |  j t g |  j � � |  _ g  t |  j � D] } d ^ qM g |  _ d |  _	 t
 |  _ d  S(   Ng        (   R	   R   R   R   R   R   R   t   rangeR   R   t   Truet   change_dimensionality(   R   R   t   _(    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR   \   s
    -(	c         G` sE   |  j  d 7_  d d |  j t t d | � t | d d � � S(   Ni   g      �?i
   g       @(   R   R   R   R   R   (   R   R   R   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR    e   s    (   R!   R"   R#   R   R    (    (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR$   B   s   	t   WayburnSeader01c           B` s#   e  Z d  Z d d � Z d �  Z RS(   s�  
    Wayburn and Seader 1 objective function.

    This class defines the Wayburn and Seader 1 [1]_ global optimization
    problem. This is a unimodal minimization problem defined as follows:

    .. math::

        f_{\text{WayburnSeader01}}(x) = (x_1^6 + x_2^4 - 17)^2
                                        + (2x_1 + x_2 - 4)^2


    with :math:`x_i \in [-5, 5]` for :math:`i = 1, 2`.

    *Global optimum*: :math:`f(x) = 0` for :math:`x = [1, 2]`

    .. [1] Jamil, M. & Yang, X.-S. A Literature Survey of Benchmark Functions
    For Global Optimization Problems Int. Journal of Mathematical Modelling
    and Numerical Optimisation, 2013, 4, 150-194.
    i   c         C` sv   t  j |  | � t t d g |  j d g |  j � � |  _ d d g d d g f |  _ d d g g |  _ d |  _ d  S(   Ng      �g      @i����i   g      �?g       @g        (	   R	   R   R   R   R   R   t   custom_boundsR   R   (   R   R   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR   �   s
    ,c         G` sK   |  j  d 7_  | d d | d d d d d | d | d d d S(   Ni   i    i   i   i   i   (   R   (   R   R   R   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR    �   s    (   R!   R"   R#   R   R    (    (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR)   k   s   	t   WayburnSeader02c           B` s#   e  Z d  Z d d � Z d �  Z RS(   s�  
    Wayburn and Seader 2 objective function.

    This class defines the Wayburn and Seader 2 [1]_ global optimization
    problem. This is a unimodal minimization problem defined as follows:

    .. math::

        f_{\text{WayburnSeader02}}(x) = \left[ 1.613 - 4(x_1 - 0.3125)^2
                                        - 4(x_2 - 1.625)^2 \right]^2
                                        + (x_2 - 1)^2


    with :math:`x_i \in [-500, 500]` for :math:`i = 1, 2`.

    *Global optimum*: :math:`f(x) = 0` for :math:`x = [0.2, 1]`

    .. [1] Jamil, M. & Yang, X.-S. A Literature Survey of Benchmark Functions
    For Global Optimization Problems Int. Journal of Mathematical Modelling
    and Numerical Optimisation, 2013, 4, 150-194.
    i   c         C` sv   t  j |  | � t t d g |  j d g |  j � � |  _ d d g d d g f |  _ d d g g |  _ d |  _ d  S(   Ng     @�g     @@i����i   g�������?g      �?g        (	   R	   R   R   R   R   R   R*   R   R   (   R   R   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR   �   s    c         G` s[   |  j  d 7_  d d | d d d d | d d d d } | d d d } | | S(   Ni   g+����?i   i    g      �?i   g      �?(   R   (   R   R   R   t   ut   v(    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR    �   s    2(   R!   R"   R#   R   R    (    (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR+   �   s   
t   Weierstrassc           B` s#   e  Z d  Z d d � Z d �  Z RS(   s�  
    Weierstrass objective function.

    This class defines the Weierstrass [1]_ global optimization problem.
    This is a multimodal minimization problem defined as follows:

    .. math::

       f_{\text{Weierstrass}}(x) = \sum_{i=1}^{n} \left [
                                   \sum_{k=0}^{kmax} a^k \cos 
                                   \left( 2 \pi b^k (x_i + 0.5) \right) - n
                                   \sum_{k=0}^{kmax} a^k \cos(\pi b^k) \right ]


    Where, in this exercise, :math:`kmax = 20`, :math:`a = 0.5` and
    :math:`b = 3`.

    Here, :math:`n` represents the number of dimensions and
    :math:`x_i \in [-0.5, 0.5]` for :math:`i = 1, ..., n`.

    *Global optimum*: :math:`f(x) = 4` for :math:`x_i = 0` for
    :math:`i = 1, ..., n`

    .. [1] Mishra, S. Global Optimization by Differential Evolution and
    Particle Swarm Methods: Evaluation on Some Benchmark Functions.
    Munich Personal RePEc Archive, 2006, 1005

    TODO line 1591.
    TODO Jamil, Gavana have got it wrong.  The second term is not supposed to
    be included in the outer sum. Mishra code has it right as does the
    reference referred to in Jamil#166.
    i   c         C` sz   t  j |  | � t t d g |  j d g |  j � � |  _ g  t |  j � D] } d ^ qL g |  _ d |  _ t	 |  _
 d  S(   Ng      �g      �?g        i    (   R	   R   R   R   R   R   R%   R   R   R&   R'   (   R   R   R(   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR   �   s
    ,(	c   	      G` s�   |  j  d 7_  d } d	 \ } } t t | d � � j } | | t d t | | | d � } |  j t | | j t t | | j � � } t t | d d �� | S(
   Ni   i   g      �?g      @g      �?i   R   i    (   g      �?g      @(   R   R   R   R   R   R   R   R   (	   R   R   R   t   kmaxR   t   bR   R   R   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR    �   s    (/(   R!   R"   R#   R   R    (    (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR.   �   s   !	t   Whitleyc           B` s#   e  Z d  Z d d � Z d �  Z RS(   s1  
    Whitley objective function.

    This class defines the Whitley [1]_ global optimization problem. This
    is a multimodal minimization problem defined as follows:

    .. math::

        f_{\text{Whitley}}(x) = \sum_{i=1}^n \sum_{j=1}^n
                                \left[\frac{(100(x_i^2-x_j)^2
                                + (1-x_j)^2)^2}{4000} - \cos(100(x_i^2-x_j)^2
                                + (1-x_j)^2)+1 \right]


    Here, :math:`n` represents the number of dimensions and
    :math:`x_i \in [-10.24, 10.24]` for :math:`i = 1, ..., n`.

    *Global optimum*: :math:`f(x) = 0` for :math:`x_i = 1` for
    :math:`i = 1, ..., n`

    .. [1] Gavana, A. Global Optimization Benchmarks and AMPGO retrieved 2015

    TODO Jamil#167 has '+ 1' inside the cos term, when it should be outside it.
    i   c         C` s�   t  j |  | � t t d g |  j d g |  j � � |  _ d d g d d g f |  _ g  t |  j � D] } d ^ qg g |  _ d |  _	 t
 |  _ d  S(   Ng{�G�z$�g{�G�z$@i����i   g      �?g        (   R	   R   R   R   R   R   R*   R%   R   R   R&   R'   (   R   R   R(   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR     s    (	c         G` st   |  j  d 7_  | } t | � j } d | d | d | d } | d d t | � d } t t | d d �� S(   Ni   g      Y@g       @g      �?g     @�@R   i    (   R   R   R   R   R   (   R   R   R   t   XIt   XJt   tempR   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR      s    (   R!   R"   R#   R   R    (    (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR1   �   s   t   Wolfec           B` s#   e  Z d  Z d d � Z d �  Z RS(   sQ  
    Wolfe objective function.

    This class defines the Wolfe [1]_ global optimization problem. This
    is a multimodal minimization problem defined as follows:

    .. math::

       f_{\text{Wolfe}}(x) = \frac{4}{3}(x_1^2 + x_2^2 - x_1x_2)^{0.75} + x_3


    with :math:`x_i \in [0, 2]` for :math:`i = 1, 2, 3`.

    *Global optimum*: :math:`f(x) = 0` for :math:`x = [0, 0, 0]`

    .. [1] Jamil, M. & Yang, X.-S. A Literature Survey of Benchmark Functions
    For Global Optimization Problems Int. Journal of Mathematical Modelling
    and Numerical Optimisation, 2013, 4, 150-194.
    i   c         C` sq   t  j |  | � t t d g |  j d g |  j � � |  _ g  t |  j � D] } d ^ qL g |  _ d |  _ d  S(   Ng        g       @(	   R	   R   R   R   R   R   R%   R   R   (   R   R   R(   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR   :  s    ,(c         G` sG   |  j  d 7_  d | d d | d d | d | d d | d S(   Ni   i   i   i    i   g      �?gUUUUUU�?(   R   (   R   R   R   (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR    B  s    (   R!   R"   R#   R   R    (    (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyR5   $  s   N(   t
   __future__R    R   R   t   numpyR   R   R   R   R   R   t   go_benchmarkR	   R
   R$   R)   R+   R.   R1   R5   (    (    (    s/   benchmarks/go_benchmark_functions/go_funcs_W.pyt   <module>   s   .:)&*81