from nlf import *

def checkAll(e=1e-3, display=True):
    """
    Checks whether the problems in the list are correctly specified.
    Parameters:
        e   :   Tolerance
        display : If True, the results will be printed

    Returns:
        ca  : True if all the problems are correctly specified
    """
    problems = ['Ferraris_tronconi', 'Himmelblau', 'Robot_kinematics', 'Wang_Wang', 'Yang_Cheng_Tong_Deng2',
                'Auto_Steering', 'Brown', 'CSTR', 'Trig', 'Yang_Cheng_Tong_Deng1']

    def check(bm):
        for root in bm.roots:
            if bm.fun(root)>e:
                return False
        return True
    if display:
        print("Checking whether problems are specified correctly")
    ca = True
    for problem in problems:
        c = check(eval(problem+'()'))
        if display:
            print("{0}:\t{1}".format(problem, c))
        if not c:
            ca = False
    if display:
        print("\nAll tests true" if ca else "Some tests failed")
    return ca


def compare(bm, alg, e=1e-2, display=True):
    """
    Tests how many roots an global optimiser can find.
    Parameters:
        bm      : An instance of a child class of the Benchmark class
        alg     : A function that can be passed the function to be optimised
                  and a list of (min, max) bounds for each variable
        e       : Tolerance
        display : If True, the results will be printed

    Returns:
        counter : A list with the number of times each root has been found.
                  Could contain extra entries if new roots are found.
    """
    ans = alg(bm.fun, bm.bounds)
    x, xl = ans.x, list(ans.xl)
    xl.append(x)
    nroots = len(bm.roots)
    counter = [0]*nroots
    for x in xl:
        if bm.fun(x)>e:
            continue
        exists = True
        for root, i in zip(bm.roots, range(len(bm.roots))):
            if functools.reduce(operator.and_, [abs(v-r)<e for v,r in zip(x, root)], True):
                counter[i]+=1
                exists = False
        if exists:
            bm.roots.append(x)
            counter.append(1)
    if display:
        count1 = functools.reduce(operator.add, [1 if v>0 else 0 for v in counter[:nroots]], 0)
        count2 = functools.reduce(operator.add, [1 if v>0 else 0 for v in counter[nroots:]], 0)
        print("The algorithm managed to find {0} of {1} root(s). As well as {2} new root(s).".format(count1, nroots, count2))
        print("Number of function evaluations: {0}".format(bm.nfev))
        print("Roots: ")
        for root,i in zip(bm.roots[:nroots], range(nroots)):
            print("{0}\t{1}".format("Found\t" if counter[i]>0 else "Not found", root))
        print("New roots:")
        for root in bm.roots[nroots:]:
            print("{0}".format(root))
    return counter


if __name__=='__main__':
    checkAll()
